﻿namespace cs_dll_injector
{
    partial class MainWindow
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.procList = new System.Windows.Forms.ListBox();
            this.injectBtn = new Guna.UI2.WinForms.Guna2Button();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.borderless = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.refreshBtn = new Guna.UI2.WinForms.Guna2Button();
            this.scroll = new Guna.UI2.WinForms.Guna2VScrollBar();
            this.headerLbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // procList
            // 
            this.procList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.procList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.procList.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.procList.ForeColor = System.Drawing.Color.White;
            this.procList.FormattingEnabled = true;
            this.procList.IntegralHeight = false;
            this.procList.ItemHeight = 19;
            this.procList.Items.AddRange(new object[] {
            "test"});
            this.procList.Location = new System.Drawing.Point(23, 57);
            this.procList.Name = "procList";
            this.procList.Size = new System.Drawing.Size(195, 283);
            this.procList.TabIndex = 1;
            // 
            // injectBtn
            // 
            this.injectBtn.Animated = true;
            this.injectBtn.BorderRadius = 12;
            this.injectBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.injectBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.injectBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.injectBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.injectBtn.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.injectBtn.ForeColor = System.Drawing.Color.White;
            this.injectBtn.Location = new System.Drawing.Point(12, 360);
            this.injectBtn.Name = "injectBtn";
            this.injectBtn.Size = new System.Drawing.Size(159, 49);
            this.injectBtn.TabIndex = 2;
            this.injectBtn.Text = "INJECT";
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.guna2PictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox1.BackgroundImage")));
            this.guna2PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.guna2PictureBox1.FillColor = System.Drawing.Color.Transparent;
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(13, 47);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(213, 305);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox1.TabIndex = 3;
            this.guna2PictureBox1.TabStop = false;
            this.guna2PictureBox1.UseTransparentBackground = true;
            // 
            // borderless
            // 
            this.borderless.BorderRadius = 5;
            this.borderless.ContainerControl = this;
            this.borderless.DockIndicatorTransparencyValue = 1D;
            this.borderless.DragStartTransparencyValue = 1D;
            this.borderless.TransparentWhileDrag = true;
            // 
            // refreshBtn
            // 
            this.refreshBtn.Animated = true;
            this.refreshBtn.BorderRadius = 12;
            this.refreshBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.refreshBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.refreshBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.refreshBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.refreshBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.refreshBtn.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.refreshBtn.ForeColor = System.Drawing.Color.White;
            this.refreshBtn.Image = ((System.Drawing.Image)(resources.GetObject("refreshBtn.Image")));
            this.refreshBtn.ImageSize = new System.Drawing.Size(26, 26);
            this.refreshBtn.Location = new System.Drawing.Point(177, 360);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(49, 49);
            this.refreshBtn.TabIndex = 4;
            // 
            // scroll
            // 
            this.scroll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.scroll.BindingContainer = this.procList;
            this.scroll.BorderRadius = 4;
            this.scroll.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.scroll.InUpdate = false;
            this.scroll.LargeChange = 10;
            this.scroll.Location = new System.Drawing.Point(200, 57);
            this.scroll.Name = "scroll";
            this.scroll.ScrollbarSize = 18;
            this.scroll.Size = new System.Drawing.Size(18, 283);
            this.scroll.TabIndex = 5;
            this.scroll.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            // 
            // headerLbl
            // 
            this.headerLbl.AutoSize = true;
            this.headerLbl.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerLbl.Location = new System.Drawing.Point(18, 19);
            this.headerLbl.Name = "headerLbl";
            this.headerLbl.Size = new System.Drawing.Size(82, 25);
            this.headerLbl.TabIndex = 6;
            this.headerLbl.Text = "Injector";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.ClientSize = new System.Drawing.Size(237, 419);
            this.Controls.Add(this.scroll);
            this.Controls.Add(this.headerLbl);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.injectBtn);
            this.Controls.Add(this.procList);
            this.Controls.Add(this.guna2PictureBox1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainWindow";
            this.Text = "Basic C# Injector";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox procList;
        private Guna.UI2.WinForms.Guna2Button injectBtn;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private Guna.UI2.WinForms.Guna2BorderlessForm borderless;
        private Guna.UI2.WinForms.Guna2Button refreshBtn;
        private Guna.UI2.WinForms.Guna2VScrollBar scroll;
        private System.Windows.Forms.Label headerLbl;
    }
}

