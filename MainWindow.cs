﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using cs_dll_injector.Functional;

namespace cs_dll_injector
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Dynamic Link Libs|*.dll";
            openFileDialog.RestoreDirectory = true;
            Injection injector = new Injection();
            injectBtn.Click += (s, a) =>
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string selectedDllPath = openFileDialog.FileName;
                    injector.injectDll(selectedDllPath, procList);
                }
            };
            refreshBtn.Click += (s, a) =>
            {
                PopulateProcessList();
                scroll.Maximum = procList.Items.Count;
            };
        }


        private void PopulateProcessList()
        {
            Process[] processes = Process.GetProcesses();
            List<string> processNames = new List<string>();
            foreach (Process process in processes)
                processNames.Add(process.ProcessName);
            processNames.Sort();
            procList.DataSource = processNames;
        }
        private void Form1_Load(object sender, EventArgs e) => PopulateProcessList();

    }
}
